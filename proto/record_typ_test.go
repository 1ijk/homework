package proto

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	textCases = []RecordTyp{Debit, Credit, StartAutopay, EndAutopay, 4}
)

func TestNewRecordTyp(t *testing.T) {
	for b, typ := range textCases {
		data, err := NewRecordTyp(byte(b))

		if b > 3 {
			assert.Error(t, err)
			continue
		}

		assert.NoError(t, err)
		assert.Equal(t, typ, data)
	}
}

func TestRecordTyp_Bytes(t *testing.T) {
	for b, typ := range textCases {
		bs := typ.Bytes()

		assert.Equal(t, bs, []byte{byte(b)})
	}
}
