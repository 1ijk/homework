package proto

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	headerBytes, _ = exampleHeader.Bytes()
	recordBytes, _ = exampleRecord.Bytes()
	exampleData    = append(headerBytes, recordBytes...)

	exampleTxLog = &MPS7{exampleHeader, []Record{exampleRecord}}
)

func TestLoad(t *testing.T) {
	buf := bytes.NewBuffer(exampleData)
	data, err := Load(buf)
	assert.NoError(t, err)
	assert.Equal(t, exampleTxLog, data)
}
