package proto

import (
	"encoding/binary"
	"fmt"
	"math"
)

// Record in the transaction log has the structure:
// | 1 byte record type enum | 4 byte (uint32) Unix timestamp | 8 byte (uint64) user ID |
type Record struct {
	RecordTyp
	Timestamp uint32
	UserID    uint64
	Amount    float64
}

func NewRecord(b []byte) (Record, error) {
	var r Record
	if !checkRecordSize(b) {
		return r, fmt.Errorf("invalid record length")
	}

	typ, err := NewRecordTyp(b[0])
	if err != nil {
		return r, err
	}

	r.RecordTyp = typ
	r.Timestamp = binary.BigEndian.Uint32(b[1:5])
	r.UserID = binary.BigEndian.Uint64(b[5:13])
	if r.HasAmount() {
		amount := binary.BigEndian.Uint64(b[13:])
		r.Amount = math.Float64frombits(amount)
	}

	return r, nil
}

func (r Record) HasAmount() bool {
	return r.RecordTyp == Debit || r.RecordTyp == Credit
}

func (r Record) Bytes() (b []byte, err error) {
	b = append(b, r.RecordTyp.Bytes()...)
	b = append(b, r.TimestampBytes()...)
	b = append(b, r.UserIDBytes()...)
	b = append(b, r.AmountBytes()...)

	if !checkRecordSize(b) {
		err = fmt.Errorf("invalid record length")
	}

	return
}

func (r Record) TimestampBytes() []byte {
	b := make([]byte, 4)
	binary.BigEndian.PutUint32(b, r.Timestamp)
	return b
}

func (r Record) AmountBytes() []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, math.Float64bits(r.Amount))
	return b
}

func (r Record) UserIDBytes() []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(r.UserID))
	return b
}

func TotalDebitDollars(records []Record) float64 {
	var total float64

	for i := 0; i < len(records); i++ {
		if records[i].IsDebit() {
			total += records[i].Amount
		}
	}

	return total
}

func TotalCreditDollars(records []Record) float64 {
	var total float64

	for i := 0; i < len(records); i++ {
		if records[i].IsCredit() {
			total += records[i].Amount
		}
	}

	return total
}

func TotalAutopayStarts(records []Record) int {
	var total int

	for i := 0; i < len(records); i++ {
		if records[i].IsStartAutopay() {
			total++
		}
	}

	return total
}

func TotalAutopayStops(records []Record) int {
	var total int

	for i := 0; i < len(records); i++ {
		if records[i].IsEndAutopay() {
			total++
		}
	}

	return total
}

func UserBalance(userID uint64, records []Record) float64 {
	var total float64

	for i := 0; i < len(records); i++ {
		if records[i].UserID != userID {
			continue
		}

		switch {
		case records[i].IsDebit():
			total -= records[i].Amount
		case records[i].IsCredit():
			total += records[i].Amount
		}
	}

	return total
}

func checkRecordSize(b []byte) bool {
	return len(b) >= 13 && len(b) <= 21
}
