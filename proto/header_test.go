package proto

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var exampleHeader = Header{MagicString, 1, 72}

func TestHeader_MagicBytes(t *testing.T) {
	data := exampleHeader.MagicBytes()
	assert.Equal(t, MagicBytes, data)
}

func TestHeader_VersionBytes(t *testing.T) {
	data := exampleHeader.VersionBytes()
	assert.Equal(t, []byte{1}, data)
}

func TestHeader_RecordCountBytes(t *testing.T) {
	data := exampleHeader.RecordCountBytes()
	assert.Equal(t, []byte{0, 0, 0, 72}, data)
}

func TestHeader_Bytes(t *testing.T) {
	data, err := exampleHeader.Bytes()
	assert.NoError(t, err)
	assert.Len(t, data, 9)
	assert.Equal(t, append(MagicBytes, 0x1, 0, 0, 0, 72), data)
}

func TestHeader_Len(t *testing.T) {
	assert.Equal(t, 72, exampleHeader.Len())
}

func TestNewHeader(t *testing.T) {
	header, err := NewHeader([]byte{77, 80, 83, 55, 1, 0, 0, 0, 72})
	assert.NoError(t, err)
	assert.Equal(t, exampleHeader, header)
}
