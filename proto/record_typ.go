package proto

import "fmt"

// RecordTyp associated with each Record
type RecordTyp byte

const (
	// Debit is RecordTyp value 0x00
	Debit RecordTyp = iota
	// Credit is RecordTyp value 0x00
	Credit
	// StartAutopay is RecordTyp value 0x00
	StartAutopay
	// EndAutopay is RecordTyp value 0x00
	EndAutopay
)

func NewRecordTyp(b byte) (RecordTyp, error) {
	switch RecordTyp(b) {
	case Debit:
		return Debit, nil
	case Credit:
		return Credit, nil
	case StartAutopay:
		return StartAutopay, nil
	case EndAutopay:
		return EndAutopay, nil
	default:
		return RecordTyp(b), fmt.Errorf("unknown record type: %v", b)
	}
}

func (rt RecordTyp) Bytes() []byte {
	return []byte{byte(rt)}
}

func (rt RecordTyp) IsDebit() bool {
	return rt == Debit
}

func (rt RecordTyp) IsCredit() bool {
	return rt == Credit
}

func (rt RecordTyp) IsStartAutopay() bool {
	return rt == StartAutopay
}

func (rt RecordTyp) IsEndAutopay() bool {
	return rt == EndAutopay
}
