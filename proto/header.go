package proto

import (
	"encoding/binary"
	"fmt"
)

const MagicString = "MPS7"

var MagicBytes = []byte(MagicString)

// Header in the transaction log has the structure:
// | 4 byte magic string "MPS7" | 1 byte version | 4 byte (uint32) # of records |
type Header struct {
	Magic       string
	Version     int
	RecordCount uint32
}

func NewHeader(b []byte) (Header, error) {
	var h Header
	if !checkHeaderSize(b) {
		return h, fmt.Errorf("invalid header length: %d", len(b))
	}

	h.Magic = string(b[0:4])
	h.Version = int(b[4])
	h.RecordCount = binary.BigEndian.Uint32(b[5:])

	return h, nil
}

func (h Header) VersionBytes() []byte {
	return []byte{byte(h.Version)}
}

func (h Header) RecordCountBytes() []byte {
	b := make([]byte, 4)
	binary.BigEndian.PutUint32(b, h.RecordCount)
	return b
}

func (h Header) MagicBytes() []byte {
	return []byte(h.Magic)
}

func (h Header) Bytes() ([]byte, error) {
	var data []byte
	data = append(data, h.MagicBytes()...)
	data = append(data, h.VersionBytes()...)
	data = append(data, h.RecordCountBytes()...)

	return data, nil
}

func (h Header) Len() int {
	return int(h.RecordCount)
}

func checkHeaderSize(b []byte) bool {
	return len(b) == 9
}
