package proto

import (
	"bufio"
	"io"
	"os"
)

func LoadFromFile(path string) (*MPS7, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	return Load(file)
}

func loadHeader(file *bufio.Reader) (h Header, err error) {
	var data []byte

	for i := 0; i < 9; i++ {
		var b byte
		b, err = file.ReadByte()
		if err != nil {
			return
		}
		data = append(data, b)
	}

	h, err = NewHeader(data)
	return
}

func loadRecord(file *bufio.Reader) (r Record, err error) {
	var data []byte
	var max int

	b, err := file.Peek(1)
	if err != nil {
		return
	}
	typ, err := NewRecordTyp(b[0])
	if err != nil {
		return
	}
	switch typ {
	case Debit, Credit:
		max = 21
	default:
		max = 13
	}

	for i := 0; i < max; i++ {
		var b byte
		b, err = file.ReadByte()
		if err != nil {
			return
		}
		data = append(data, b)
	}

	return NewRecord(data)
}

func Load(file io.Reader) (*MPS7, error) {
	data := bufio.NewReader(file)

	var txLog MPS7

	header, err := loadHeader(data)
	if err != nil {
		return &txLog, err
	}

	txLog.Header = header

	for {
		record, err := loadRecord(data)
		if err == io.EOF {
			break
		}
		if err != nil {
			return &txLog, err
		}

		txLog.Records = append(txLog.Records, record)
	}

	return &txLog, nil
}
