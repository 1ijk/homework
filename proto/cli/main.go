package main

import (
	"flag"
	"fmt"
	"gitlab.com/1ijk/homework/proto"
	"io"
	"log"
)

var userID uint64

func main() {
	flag.Uint64Var(&userID, "userID", 0, "userID to get balance")
	flag.Parse()

	filenames := flag.Args()

	var data proto.MPS7
	for i := 0; i < len(filenames); i++ {
		records, err := proto.LoadFromFile(filenames[i])
		if err == io.EOF {
			continue
		}
		if err != nil {
			log.Fatal(err)
		}

		data.Records = append(data.Records, records.Records...)
	}

	fmt.Printf("What is the total amount in dollars of debits? $%.2f\n", proto.TotalDebitDollars(data.Records))
	fmt.Printf("What is the total amount in dollars of credits? $%.2f\n", proto.TotalCreditDollars(data.Records))
	fmt.Printf("How many autopays were started? %d\n", proto.TotalAutopayStarts(data.Records))
	fmt.Printf("How many autopays were ended? %d\n", proto.TotalAutopayStops(data.Records))

	if userID != 0 {
		fmt.Printf("What is balance of user ID %d? $%.2f\n", userID, proto.UserBalance(userID, data.Records))
	}
}
