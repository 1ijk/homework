package proto

import (
	"encoding/binary"
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
)

var (
	exampleRecord = Record{Debit, 1393108945, 4136353673894269217, 604.274335557087}
)

func TestRecord_TimestampBytes(t *testing.T) {
	data := exampleRecord.TimestampBytes()
	assert.Len(t, data, 4)

	value := binary.BigEndian.Uint32(data)
	assert.Equal(t, exampleRecord.Timestamp, value)
}

func TestRecord_UserIDBytes(t *testing.T) {
	data := exampleRecord.UserIDBytes()
	assert.Len(t, data, 8)

	value := binary.BigEndian.Uint64(data)
	assert.Equal(t, exampleRecord.UserID, value)
}

func TestRecord_AmountBytes(t *testing.T) {
	data := exampleRecord.AmountBytes()
	assert.Len(t, data, 8)

	value := binary.BigEndian.Uint64(data)
	assert.Equal(t, exampleRecord.Amount, math.Float64frombits(value))
}

func TestRecord_Bytes(t *testing.T) {
	data, err := exampleRecord.Bytes()
	assert.NoError(t, err)
	assert.Len(t, data, 21)

	assert.Equal(t, []byte{data[0]}, exampleRecord.RecordTyp.Bytes())
	assert.Equal(t, data[1:5], exampleRecord.TimestampBytes())
	assert.Equal(t, data[5:13], exampleRecord.UserIDBytes())
	assert.Equal(t, data[13:], exampleRecord.AmountBytes())
}

func TestNewRecord(t *testing.T) {
	data, err := exampleRecord.Bytes()
	assert.NoError(t, err)

	record, err := NewRecord(data)
	assert.NoError(t, err)
	assert.Equal(t, exampleRecord.RecordTyp.Bytes(), record.RecordTyp.Bytes())
	assert.Equal(t, exampleRecord.TimestampBytes(), record.TimestampBytes())
	assert.Equal(t, exampleRecord.UserIDBytes(), record.UserIDBytes())
	assert.Equal(t, exampleRecord.AmountBytes(), record.AmountBytes())
}
